FROM python:3.8

WORKDIR /app
COPY . .
RUN pip install -r requirments.txt

ENTRYPOINT ["python"]
CMD ["app.py"]

