import sqlite3

conn = sqlite3.connect('data.db')
print("Opened database successfully")

cursor = conn.cursor()

create_table = "CREATE TABLE IF NOT EXISTS users (id INTEGER PRIMARY KEY, username text, password text)"
cursor.execute(create_table)
print("users Table created successfully")


create_item_table = "CREATE TABLE IF NOT EXISTS items (name text PRIMARY KEY, price real)"
cursor.execute(create_item_table)
print("items Table created successfully")

conn.commit()
conn.close()
